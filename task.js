/**
 * 1. Напиши функцию-конструктор, которая называется CreateColor.
 * Функция принимает на вход строку с названием цвета и возвращает объект,
 * в котором есть:
 * - поле colorName с названием цвета
 * - метод use, возвращающий строку 'Используется {color} цвет'.
 *      Название цвета должно быть написано маленькими буквами
 * - метод stopUse, возвращающий строку '{color} цвет больше не используется'.
 *      Название цвета должно начинаться с заглавной буквы, а остальные буквы — маленькие.
 *
 * Цвет в методах должен получаться с использованием ключевого слова this.
 */

function CreateColor(colorName) {
    return {
        colorName: colorName,
        use: function () {
            return `Используется ${this.colorName.toLowerCase()} цвет`;
        },
        stopUse: function () {
            return `${this.colorName.charAt(0).toUpperCase() + this.colorName.slice(1).toLowerCase()} цвет больше не используется`;
        }
    }
}

/**
 * 2. Напиши функции, которые возвращают результат вызова метода use из прошлой функции,
 * в контексте нового объекта {colorName: 'серо-буро-малиновый в крапинку'}.
 * Сделай это следующими путями:
 * 1) Добавив к этому объекту метод use (который надо позаимствовать из результата вызова функции createColor)
 * 2) Воспользовавшись методом call
 * 3) Воспользовавшись методом bind
 */

function useColor1() {
  // добавить use к объекту newcolor. Нужно реиспользовать use из объекта blueColor.
  const newcolor = { colorName: 'серо-буро-малиновый в крапинку' };
  const blueColor = new CreateColor('Синий');

  newcolor.use = blueColor.use;
  return newcolor.use();
}

function useColor2() {
  // Воспользуйся методом call
  const newcolor = { colorName: 'серо-буро-малиновый в крапинку' };
  return new CreateColor().use.call(newcolor);
}

function useColor3() {
  // Воспользуйся методом bind
  const newcolor = { colorName: 'серо-буро-малиновый в крапинку' };
  return new CreateColor().use.bind(newcolor)();
}

/**
 * 3. Допиши функцию-конструктор Song.
 * Вернувшийся из конструктора объект должен содержать поля:
 *  - title, author, album — содержат соответствующие значения из аргументов
 *  - getFullName - метод, возвращающий название в виде строки:
 *  "композиция «{title}», исполнитель {author}, альбом «{album}»"
 *  - setYear - метод, принимающий на вход аргумент года издания
 *  и добавляющий год издания объекту песни
 *  - setTitle - метод, принимающий на вход аргумент с новым названием песни и
 *  заменяющий старое название на новое
 */

function Song(title, author, album) {
    return {
        title: title,
        author: author,
        album: album,
        getFullName: function () {
            return `композиция «${this.title}», исполнитель ${this.author}, альбом «${album}»`
        },
        setYear: function (year) {
            this.year = parseInt(year);
        },
        setTitle: function (title) {
            this.title = title;
        }
    }
}

module.exports = {
  CreateColor,
  useColor: {
    useColor1,
    useColor2,
    useColor3,
  },
  Song,
};